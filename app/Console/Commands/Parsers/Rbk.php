<?php

namespace App\Console\Commands\Parsers;

use App\Helpers\Parser;
use Illuminate\Console\Command;

class Rbk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbk:getNews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The command get news from https://www.rbc.ru/ and saves them';

    private $parser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parser->getLayoutWebsite();
        dd('trabajo');
    }
}
