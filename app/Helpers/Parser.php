<?php

namespace App\Helpers;

use App\Enums\RbkDTO;
use GuzzleHttp\Client;

/**
 * Class Parser
 * @package App\Helpers
 */
class Parser
{
    /**
     * The guzzle client
     *
     * @var Client
     */
    private $guzzle;

    /**
     * The rbk enum
     *
     * @var RbkDTO
     */
    private $rbk;

    /**
     * Parser constructor.
     * @param Client $guzzle
     * @param RbkDTO $rbk
     */
    public function __construct(Client $guzzle, RbkDTO $rbk)
    {
        $this->guzzle = $guzzle;
        $this->rbk = $rbk;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLayoutWebsite()
    {
        $this->rbk->layout = $this->guzzle
            ->request('GET', 'https://www.rbc.ru/')
            ->getBody()->__toString();
    }
}
