<?php

namespace App\Enums;

/**
 * Class Rbk
 * @package App\Enums
 */
class RbkDTO
{
    /**
     * The layout of https://www.rbc.ru/
     *
     * @var string
     */
    public $layout;

    /**
     * @param $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }
}
